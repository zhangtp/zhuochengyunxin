<?php
class WrittenTestResults
{
	public static $Questions = [
		'1' => '1:变量$a, $b, $c随机true或者false，请在$a为true，并且$b或$c有一个为true情况下输出”hello world”',
		'2' => '2:"S-3341"、"UX331"、"KL^72T"如何求其中数字之和。 (3341+331+72) ',
		'3' => '3:修改下面数组$arr中键“value”的值，让其值等于原来值的平方（例如：11.2变成11.2x11.2）',
		'4' => '4:已知某校数据库有两个表, Student(学生), Class(班级),请编用SQL符合下列两处场景:',
		'4-1' => '1:找出还未选出班长的班级',
		'4-2' => '2:某天有若干同学退学了, 把已退学的同学从班级列表删除',
		'fj1' => '附加题1：,Given an array nums, write a function to move all 0\'s to the end of it while maintaining the relative order of the non-zero elements.For example, given nums = [0, 1, 0, 42 ,3], after calling your function, nums should be [1, 42, 3, 0, 0].Note:1.	You must do this in-place without making a copy of the array. 2.	Minimize the total number of operations ',
		'fj2' => '附加题2：在网页上显示一个与服务器同步的时间(口述) ps:JS时间函数不精确',
	];

	public static function answers()
	{
		self::first();
		self::second();
		self::third();
		self::fourth();
		self::fuJia();
	}

	private static function first()
    {
    	$question = self::getQuestion('1');
    	$answer = <<<EOF
    	【问题】: {$question}
    	【答案】: 
		if(\$a && (\$b || \$c))
			echo "hello world";

EOF;
    	return self::response($answer);
    }

    private static function second()
    {
    	$question = self::getQuestion('2');
    	$answer = <<<EOF
    	【问题】: {$question}
    	【答案】: 
		\$arr = ["S-3341", "UX331", "KL^72T"];
		\$pattern = '/\d+/';
		\$number = 0;
		foreach (\$arr as \$b => \$v) {
		preg_match_all(\$pattern,\$v, \$c);
		if(isset(\$c[0][0]))
			\$number += (int)\$c[0][0];
		}
		echo \$number;

EOF;
    	return self::response($answer);
    }
    
    private static function third()
    {
    	$question = self::getQuestion('3');
    	$answer = <<<EOF
    	【问题】: {$question}
    	【答案】: 
		\$arr = [['id' => 1,'sub' =>[['value' => 12.3],['value' => 11.3]]],['id' => 2,'sub' =>[['value' => 10.3],['value' => 9.3]]]];
		foreach (\$arr as \$key => &\$value) {
			foreach (\$value['sub'] as \$childKey => &\$childVal) {
				\$childVal['value'] = \$childVal['value']*\$childVal['value'];
			}
		}
		var_dump(\$arr);

EOF;
    	return self::response($answer);
    }

    private static function fourth()
    {
    	$question = self::getQuestion('4');
    	$question1 = self::getQuestion('4-1');
    	$question2 = self::getQuestion('4-2');
    	$answer = <<<EOF
    	【问题】: {$question}
    	【问题-1】: {$question1}
    	【答案-1】: 
		\$sql = "SELECT distinct(class_id) FROM `class` WHERE class_id not in(SELECT c.class_id FROM `student` s LEFT JOIN `class` c ON s.student_id=c.student_id WHERE duty ='班长' AND leave = 0)";

		【问题-2】: {$question2}
    	【答案-2】: 
		\$sql = "UPDATE `student` SET leave=1 WHERE student_id IN(1,2,3)";

EOF;
    	return self::response($answer);
    }

    private static function fuJia()
    {
    	$question1 = self::getQuestion('fj1');
    	$question2 = self::getQuestion('fj2');
    	$answer = <<<EOF
    	【附加题】
    	【问题】: {$question1}
    	【答案-1】: 
		\$nums = [0, 1, 0, 42 ,3];
		foreach (\$nums as \$item => \$value) {
			if(\$value === 0)
			{
				\$nums[] = 0;
				unset(\$nums[\$item]);
			}
		}
		ksort(\$nums);

		【问题-2】: {$question2}
    	【答案-2】: 
		方案：用户请求/刷新页面时，服务器端给到前端页面一个初始时间，前端根据服务端的时间来做本地时间初始化。虽然这样时间也不是很精确（服务器响应到页面渲染完毕中间会存在一定的时间差），但是也不会受本地时间影响导致更大差异。

EOF;
    	return self::response($answer);
    }

    private static function getQuestion($item)
    {
    	// $question = self::$Questions[$item] ?? "Question Not Found!";
    	$question = isset(self::$Questions[$item]) ? self::$Questions[$item] : "Question Not Found!";
    	return $question;
    }

    private static function response($answer)
    {
    	echo $answer;
    }
}

WrittenTestResults::answers();